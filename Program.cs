﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Device.Location;
using System.Xml;
using System.Diagnostics;



namespace ExportCompegps
{

    // class extends GeoCoordiante
    // adds methods for IGC Coordinates
    class point : GeoCoordinate
    {
        public String name;
        public String shortname;

        // get point from utm coordinates
        public point(double utmX, double utmY, string utmZone)
        {

            bool isNorthHemisphere = utmZone.Last() >= 'N';

            var zone = int.Parse(utmZone.Remove(utmZone.Length - 1));
            var c_sa = 6378137.000000;
            var c_sb = 6356752.314245;
            var e2 = Math.Pow((Math.Pow(c_sa, 2) - Math.Pow(c_sb, 2)), 0.5) / c_sb;
            var e2cuadrada = Math.Pow(e2, 2);
            var c = Math.Pow(c_sa, 2) / c_sb;
            var x = utmX - 500000;
            var y = isNorthHemisphere ? utmY : utmY - 10000000;

            var s = ((zone * 6.0) - 183.0);
            var lat = y / (6366197.724 * 0.9996); // Change c_sa for 6366197.724
            var v = (c / Math.Pow(1 + (e2cuadrada * Math.Pow(Math.Cos(lat), 2)), 0.5)) * 0.9996;
            var a = x / v;
            var a1 = Math.Sin(2 * lat);
            var a2 = a1 * Math.Pow((Math.Cos(lat)), 2);
            var j2 = lat + (a1 / 2.0);
            var j4 = ((3 * j2) + a2) / 4.0;
            var j6 = (5 * j4 + a2 * Math.Pow((Math.Cos(lat)), 2)) / 3.0; // saque a2 de multiplicar por el coseno de lat y elevar al cuadrado
            var alfa = (3.0 / 4.0) * e2cuadrada;
            var beta = (5.0 / 3.0) * Math.Pow(alfa, 2);
            var gama = (35.0 / 27.0) * Math.Pow(alfa, 3);
            var bm = 0.9996 * c * (lat - alfa * j2 + beta * j4 - gama * j6);
            var b = (y - bm) / v;
            var epsi = ((e2cuadrada * Math.Pow(a, 2)) / 2.0) * Math.Pow((Math.Cos(lat)), 2);
            var eps = a * (1 - (epsi / 3.0));
            var nab = (b * (1 - epsi)) + lat;
            var senoheps = (Math.Exp(eps) - Math.Exp(-eps)) / 2.0;
            var delt = Math.Atan(senoheps / (Math.Cos(nab)));
            var tao = Math.Atan(Math.Cos(delt) * Math.Tan(nab));

            Longitude = (delt / Math.PI) * 180 + s;
            Latitude = (((lat + (1 + e2cuadrada * Math.Pow(Math.Cos(lat), 2) - (3.0 / 2.0) * e2cuadrada * Math.Sin(lat) * Math.Cos(lat) * (tao - lat)) * (tao - lat))) / Math.PI) * 180; // era incorrecto el calculo

        }

        // get point from latitude / longitude 
        public point(double latitude, double longitude, double altitude = 0, String name = "", String shortname = "")
        {
            this.Latitude = latitude; this.Longitude = longitude; this.Altitude = altitude; this.name = name; this.shortname = shortname;
        }

        // create point from IGC BRecord
        public point(string BRecord, String name = "", String shortname = "")
        {

            Latitude = float.Parse(BRecord.Substring(7, 2)) + float.Parse(BRecord.Substring(9, 2) + "." + BRecord.Substring(11, 3)) / 60;
            Longitude = float.Parse(BRecord.Substring(15, 3)) + float.Parse(BRecord.Substring(18, 2) + "." + BRecord.Substring(20, 3)) / 60;


            if (BRecord.Substring(18, 1).ToLower() == "s")
                Latitude = -Latitude;
            if (BRecord.Substring(23, 1).ToLower() == "w")
                Longitude = -Longitude;

        }

        // create a new point that is displaced by deltaLatitude / deltaLongitude
        public point move(int deltaLatitude, int deltaLongitude, int deltaAltitude)
        {
            double deltay = 90.0 * ((double)deltaLatitude / 10000000.0);
            double deltax = 180.0 * ((double)deltaLongitude / 20000000.0);
            return ( new point(Latitude + deltay, Longitude + deltax, Altitude + deltaAltitude, name, shortname));
        }

        // latitude in degrees
        public int latDegrees
        {
            get
            {
                return (int)Math.Truncate(Latitude);
            }
        }

        // longitude in degrees
        public int longDegrees
        {
            get
            {
                return (int)Math.Truncate(Longitude);
            }
        }

        // minutes latitude
        public double latMinutes
        {
            get
            {
                return ((Latitude - latDegrees) * 60);
            }
        }

        // minutes longitude
        public double longMinutes
        {
            get
            {
                return (Longitude - longDegrees) * 60;
            }
        }

        // the coordinates in IGC Format
        public string IGCCoords
        {
            get
            {
                String SUtmy = Math.Abs(latDegrees).ToString("00") + Math.Abs(latMinutes).ToString("00.000");
                SUtmy = SUtmy.Replace(".", "");
                String SUtmx = Math.Abs(longDegrees).ToString("000") + Math.Abs(longMinutes).ToString("00.000");
                SUtmx = SUtmx.Replace(".", "");
                String xPos = "";
                String yPos = "";
                if (Latitude < 0.0) yPos = "S"; else yPos = "N";
                if (Longitude < 0.0) xPos = "W"; else xPos = "E";

                String altString = Math.Abs(Altitude).ToString("00000");

                return SUtmy + yPos + SUtmx + xPos + "A" + altString + altString;
            }
        }

        // get an igc B Record
        public string IGCBRecord(DateTime theTime)
        {
            string record= "B" + theTime.ToString("HHmmss") + this.IGCCoords + "010\r\n";
            Debug.Assert(record.Length == 40);
               

            return record;
        }

        // get the closest point
        public point getClosest(List<point> points)
        {
            double distance = 20000000.0;
            point closest = null;
            for (int i = 0; i < points.Count; i++)
            {
                double newDistance = this.GetDistanceTo(points[i]);

                if (newDistance < distance) { distance = newDistance; closest = points[i]; }
            };
            return closest;
        }

    };


    class Program
    {
        // separators in igc files
        static string[] igcBRecordSep = { "\nB" };
        static string[] newLineSep = { "\n" };
        static string[] spaceSep = { " " };

        // location of files
        static string myPath = ".";
        static string logbook = myPath + "\\LOGBOOK.MDB";
        static string RootPath = myPath+"..\\flights";
        static string dhvWaypoints = myPath + "\\dhvgelaende_gpx_alle.gpx";
        static string flylandWaypoints = myPath + "\\Waypoints_Startplatz.GPX";
        static point dummy = new point(51.48, 0.001, 0, "UNKNOWN", "UNKNOWN");

        // get a waypoint with name startPlace
        public static point getClosest(List<point> points, string startPlace)
        {
            for (int i = 0; i < points.Count; i++)
            {
                if ( points[i].shortname.ToLower() == startPlace.ToLower() )
                    return points[i];
            };
            for (int i = 0; i < points.Count; i++)
            {
                if (points[i].name.ToLower() == startPlace.ToLower())
                    return points[i];
            };
            for (int i = 0; i < points.Count; i++)
            {
                if (points[i].shortname.ToLower().Contains(startPlace.ToLower()))
                    return points[i];
            };
            for (int i = 0; i < points.Count; i++)
            {
                if (points[i].name.ToLower().Contains(startPlace.ToLower()))
                    return points[i];
            };

            return null;

        }

        // read files of waypoints ( launches )
        public static List<point> readLaunches()
        {
            List<point> points = new List<point>(); 

            XmlDocument doc = new XmlDocument();

            // read the dhv waypoints
            doc.Load(dhvWaypoints);

            XmlNodeList wptNodes = doc.GetElementsByTagName("wpt");

            // read all wpt nodes
            foreach (XmlNode node in wptNodes)
            {
                double posY = float.Parse(node.Attributes["lat"].Value);
                double posX = float.Parse(node.Attributes["lon"].Value);
                double altitude = 0;
                if (node["ele"] != null) // elevation
                    altitude = float.Parse(node["ele"].InnerText);
                string name = node["name"].InnerText; // read name from node "name"
                string shortname = node["link"]["text"].InnerText; // read short name from text in html link

                point newPoint = new point(posY, posX, altitude, name, shortname);
                points.Add(newPoint);
                Console.WriteLine(points.Count);
            }

            doc = new XmlDocument();
            
            // read waypoints from flyland
            doc.Load(flylandWaypoints);

            wptNodes = doc.GetElementsByTagName("wpt");
            foreach (XmlNode node in wptNodes)
            {
                double posY = float.Parse(node.Attributes["lat"].Value);
                double posX = float.Parse(node.Attributes["lon"].Value);
                double altitude = 0;
                if (node["ele"] != null)
                    altitude = float.Parse(node["ele"].InnerText);
                string name = node["name"].InnerText;
               
                point newPoint = new point(posY, posX, altitude, name, name);
                points.Add(newPoint);

                Console.WriteLine(points.Count);
            }

            Console.WriteLine(points.Count + " lines");

            return points;

        }

        // locate a line in the igc file that contains the String "match". Replace line by parameter paramName and value newValue
        public static String patchIGC(String igc, String match, String paramName, String newValue)
        {
            string[] separator = { "\n", "\r" };


            string[] mySplit = igc.Split(separator, StringSplitOptions.RemoveEmptyEntries);

            List<string> myList = mySplit.ToList<string>();
            List<string> toRemove = new List<string>();

            igc = "";

           
            int dteIndex = -1;
            int patchIndex = -1;
            string  dteString = null;


            for (int i = 0; i < myList.Count; i++)

            {
                if (myList[i].StartsWith("HFDTE")) 
                {
                    dteIndex = i; // insert here
                    dteString = myList[i];
                }
                if (mySplit[i].ToLower().Contains(match.ToLower()))
                {   if (patchIndex == -1)
                    {
                        patchIndex = i;
                    }
                    else toRemove.Add(myList[i]); // add duplicates
                }
            }

            if (patchIndex == -1)
            {
                myList.Insert(dteIndex + 1, paramName + ":" + newValue);
            }
            else
            {
                myList[patchIndex] = paramName + ":" + newValue;
            }

            foreach(string s in myList)
            {   if ( ! toRemove.Contains(s))
                    igc = String.Concat(igc, s + "\r\n");
            }
            return igc;



        }

        // get the first point of the track
        public static point firstPoint(String igc)
        {
            int pos1 = igc.IndexOf("\nB");
            String BRecord = "";
            if (pos1 > 0)
            {
                int pos2 = igc.IndexOf("\n", pos1 + 1);
                if (pos2 > 0)
                {
                    BRecord = igc.Substring(pos1 + 1, pos2 - pos1 - 1);

                }
            }
            return new point(BRecord);
        }

        // make a dummy igc file that is seconds long, starts at time start at site Site
        public static String dummyIGC(int seconds, DateTime start, List<point> points, String Site)
        {

            point p1 = getClosest(points, Site);

            if (p1 == null)
                p1 = dummy;
            
            seconds += 120;

            int startHour = start.Hour;

            if ( startHour > 20 )
            {
                startHour = 11;
            }

            DateTime myDate = new DateTime(start.Year, start.Month, start.Day, startHour, start.Minute,start.Second);


            String igc = "HFDTE" + start.ToString("ddMMyy") + "\r\n";
            igc = String.Concat(igc, "HFSITSite:" + Site + "\r\n");
            igc = String.Concat(igc, "HFDTM100GPSDATUM:WGS84\r\n");



            bool climbing = true;


            for (int i = 0; i < seconds; i += 5)
            {
                igc = String.Concat(igc, p1.IGCBRecord(myDate.AddSeconds(i)));
                if (climbing)
                {
                    p1=p1.move(50, 50, 5);

                }
                else p1=p1.move(50, 50, -5);

                if (p1.Altitude > 3000) climbing = false;
                if (p1.Altitude < 0) climbing = true;


            }

            return igc;
        }

        // read the comment lines in igc files
        public static String igcComment(String igc)
        {
            String comment = "";

            try
            {

                string[] lines = igc.Split(newLineSep, StringSplitOptions.RemoveEmptyEntries);


                for (int i = 0; i < lines.Length; i++)

                {
                    String line = lines[i].Trim();
                    if ((line.StartsWith("L")) && (!line.StartsWith("LX")))
                    {
                        comment = string.Concat(comment, line.Substring(1) + " ");

                    }
                }
            }

            catch (Exception ex)
            {

                Console.WriteLine(ex);
            }

            return comment.Trim();
        }

        // read the comment lines in trk files
        public static String trkComment(String trk)
        {
            String comment = "";

            try
            {

                string[] lines = trk.Split(newLineSep, StringSplitOptions.RemoveEmptyEntries);


                for (int i = 0; i < lines.Length; i++)

                {
                    String line = lines[i].Trim();
                    line = System.Text.RegularExpressions.Regex.Replace(line, @"\s+", " "); // reduce multiple spaces to one

                    String[] elems = line.Split(spaceSep, StringSplitOptions.RemoveEmptyEntries);


                    if (elems.Length > 0)
                    {

                        String Type = elems[0];

                        if (Type == "M")
                        {
                            comment = String.Concat(comment, line.Substring(2) + " ");
                        }

                        
                    }
                }
            }

            catch (Exception ex)
            {

                Console.WriteLine(ex);
            }

            return comment.Trim();
        }

        // convert trk ( compegps ) to basic igc
        public static String trk2igc(String trk)
        {
            String igc = "";

            Console.WriteLine("trk length:" + trk.Length);

            try
            {

                string[] lines = trk.Split(newLineSep, StringSplitOptions.RemoveEmptyEntries);

                bool wroteHeader = false;

                String startPlace = "";

                String comment = "";

                for (int i = 0; i < lines.Length; i++)

                {
                    String line = lines[i].Trim();
                    line = System.Text.RegularExpressions.Regex.Replace(line, @"\s+", " ");

                    String[] elems = line.Split(spaceSep, StringSplitOptions.RemoveEmptyEntries);


                    if (elems.Length > 0)
                    {

                        String Type = elems[0];


                        if (Type == "D")
                        {
                            startPlace = elems[1];
                        }

                        if (Type == "M")
                        {
                            comment = String.Concat(comment, line.Substring(2) + " ");
                        }

                        if (Type == "T" && elems.Length == 17)
                        {

                            String utmZone = elems[1];
                            int utmx = 0;
                            int utmy = 0;
                            try
                            {
                                utmx = Int32.Parse(float.Parse(elems[2]).ToString("00000000"));

                                utmy = Int32.Parse(float.Parse(elems[3]).ToString("00000000"));
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex);
                            }
                            DateTime myDate = DateTime.Parse(elems[4] + " " + elems[5]);


                            point mypoint = new point(utmx, utmy, utmZone);

                            float altitude = float.Parse(elems[7]);

                            mypoint.Altitude = altitude;


                            if (!wroteHeader)
                            {
                                igc = "HFDTE" + myDate.ToString("ddMMyy") + "\r\nHFDTM100DATUM:WGS-1984\r\n";
                                igc = String.Concat(igc, "HFSITSite:" + startPlace + "\r\n");
                                igc = String.Concat(igc, "HFPLTPilot:" + comment + "\r\n");
                                wroteHeader = true;
                            }


                            igc = igc + mypoint.IGCBRecord(myDate);
                        }
                    }
                }
            }

            catch (Exception ex)
            {

                Console.WriteLine(ex);
            }



            return igc;
        }



        static void Main(string[] args)
        {
            List<point> launches = readLaunches();
            List<string> files = new List<string>();

        

            // read the mdb file

            string strAccessConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + logbook;
            string strAccessSelect = "SELECT * FROM TablaVuelos"; // table with flight data and track ( igc OR trk )

            // Create the dataset and add the Categories table to it:
            DataSet myDataSet = new DataSet();
            OleDbConnection myAccessConn = null;
            try
            {
                myAccessConn = new OleDbConnection(strAccessConn);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: Failed to create a database connection. \n{0}", ex.Message);
                return;
            }

            try
            {

                OleDbCommand myAccessCommand = new OleDbCommand(strAccessSelect, myAccessConn);
                OleDbDataAdapter myDataAdapter = new OleDbDataAdapter(myAccessCommand);

                // read the tables of flights
                myAccessConn.Open();
                myDataAdapter.Fill(myDataSet, "TablaVuelos");

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: Failed to retrieve the required data from the DataBase.\n{0}", ex.Message);
                return;
            }
            finally
            {
                myAccessConn.Close();
            }

            // A dataset can contain multiple tables, so let's get them
            // all into an array:
            DataTableCollection dta = myDataSet.Tables;

            DataColumnCollection drc = myDataSet.Tables["TablaVuelos"].Columns;
            int i = 0;
            foreach (DataColumn dc in drc)
            {
                // Print the column subscript, then the column's name
                // and its data type:
                Console.WriteLine("Column name[{0}] is {1}, of type {2}", i++, dc.ColumnName, dc.DataType);
            }
            DataRowCollection dra = myDataSet.Tables["TablaVuelos"].Rows;


            foreach (DataRow dr in dra)
            {
                
                DateTime StartTime = (DateTime)dr["HoraDespegue"]; // time of the flight
                int length = (int)dr["Duracion"]; // length in minutes
                String start = (String)dr["NombreSitio"]; // the site name
                if (start == "") start = "UNKNOWN"; // if site is empty call it "UNKNOWN" 
                String comment = (String)dr["Comentario"]; // the comment. There is also comment in the igc and trk files
                String altComment = ""; // comment within the igc or trk file
                comment = comment.Replace("\r", ""); // remove cr
                comment = comment.Replace("\n", " "); // remove nl
                String format = ((String)dr["FormatoFicheroTexto"]).ToLower(); // read the format of the track 
                if (format == "t") format = "igc"; // some tracks are of type "t". Those are igc, too
                String track = (String)dr["FicheroVuelo"]; // read the track
                Console.WriteLine("length:" + track.Length);
                // remove those quotation marks that shouldn't be there
                if (track.Length > 1)
                {
                    if (track[0] == '"') track = track.Substring(1);
                    if (track[track.Length - 1] == '"') track.Remove(track.Length - 1);
                }
                // read and format day, hour, minute and seconds
                String month = String.Format("{0,2}", StartTime.Month.ToString());
                month = month.Replace(" ", "0");
                String year = StartTime.Year.ToString("00");
                String day = StartTime.Day.ToString("00");
                String hour = StartTime.Hour.ToString("00");
                String minute = StartTime.Minute.ToString("00");
                String second = StartTime.Second.ToString("00");


                try
                {
                    if (format == "trk")
                    {
                        // read the comment from the trk
                        // and replace comment of there is a longer
                        // comment within the trk file
                        altComment = trkComment(track);
                        if (altComment.Length > comment.Length)
                            comment = altComment;
                        track = trk2igc(track);
                    }
                    else
                    {
                        // read the comment from the trk
                        // and replace comment of there is a longer
                        // comment within the igc file
                        altComment = igcComment(track);
                        if (altComment.Length > comment.Length)
                            comment = altComment;
                    }
                    // don't know what that is, but remove it
                    if (comment.Contains("XCMOZ") || comment.Contains("COMPEGPS"))
                        comment = "";
                    if (track.Length == 0)
                        Console.WriteLine("error converting trk to igc");

                    // if the igc file is short but the flightbook
                    // knows about a longer flight then replace
                    // the short track with a "dummy" track 
                    if (track.Length < 1000)
                    {
                        if (track.Length > 0)
                        {

                            String[] theLines = track.Split(igcBRecordSep, 3, StringSplitOptions.RemoveEmptyEntries);
                            if (theLines.Length < 4)
                            {
                                Console.WriteLine("short track");
                                String dateString = track.Substring(track.IndexOf("HFDTE") + 5, 6);
                                String startString = theLines[1].Substring(0, 6);
                                DateTime takeoff = new DateTime(int.Parse(dateString.Substring(4, 2)) + 2000, int.Parse(dateString.Substring(2, 2)), int.Parse(dateString.Substring(0, 2)),
                                                                    int.Parse(startString.Substring(0, 2)),
                                                                    int.Parse(startString.Substring(2, 2)), int.Parse(startString.Substring(4, 2)));
                                track = dummyIGC(length, StartTime, launches, start);
                            }
                        }
                    }


                    // replace the "pilot" name by the comment
                    // so airtome can read it
                    track = patchIGC(track, "PLTPilot", "HFPLTPilot", comment);
                    


                    // if the start is unknown
                    // then try to read it's name from the track
                    if (start == "UNKNOWN")
                    {
                        int sitePos = track.IndexOf("HFSITSite", StringComparison.CurrentCultureIgnoreCase);
                        if (sitePos > 0)
                        {
                            int siteEnd = track.IndexOf("\n", sitePos);
                            start = track.Substring(sitePos + 10, siteEnd - sitePos - 10).Trim();
                            if (start == "") start = "UNKNOWN";
                        }

                    }

                    // if the start is still unknown then take the closest 
                    // waypoint as the start
                    if (start == "UNKNOWN")
                    {
                        point fp = firstPoint(track);

                 
                        // make sure we don't assign a name to the
                        // dummy start
                        if (! ( fp.GetDistanceTo(dummy) < 1))
                        {

                            point newStart = fp.getClosest(launches);

                            Console.WriteLine("closest: " + newStart.name + " " + fp.GetDistanceTo(newStart).ToString());

                            double distance = fp.GetDistanceTo(newStart);

                            // if waypoint is more than 1000m away, then add string of format "-x.ykm" to the name
                            if (distance > 1000)
                            {
                                start = newStart.shortname + "-" + (fp.GetDistanceTo(newStart) / 1000).ToString("0.0") + "km";
                            }
                            else start = newStart.shortname;
                        }
                    }

                    // patch the igc file. Add the site name
                    track = patchIGC(track, "HFSITSite","HFSITSite", start);

                    // remove invalid filename characters
                    foreach (char x in Path.GetInvalidFileNameChars())
                    {
                        start = start.Replace(x.ToString(), "-");
                    }

                    // replace space by "-"
                    start = start.Replace(" ", "-");

                    // place the file into directory RootPath\year\month
                    String myDirectory = RootPath + "\\"+ year + "\\" + month;

                    // if necessary create the directories
                    if (!Directory.Exists(myDirectory))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(myDirectory);
                    }

                    // name the file yyyy-MM-dd_hh.mm_start-name.igc
                    // skytraxx 3.0 compatible
                    String FileName = year + "-" + month + "-" +
                   day + "_" + hour + "." + minute + "_" + start + ".igc";

                  


                    Console.WriteLine(FileName);

                    if (files.Contains(FileName))
                    {
                        Console.WriteLine("duplicate: " + FileName);
                    }
                    else
                        files.Add(FileName);

                    //Pass the filepath and filename to the StreamWriter Constructor
                    StreamWriter sw = new StreamWriter(myDirectory + "\\" + FileName, false, Encoding.GetEncoding("ISO-8859-15"));

                    sw.Write(track);

                    sw.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception: " + e.Message);
                }
                finally
                {
                    Console.WriteLine("Executing finally block.");
                }



            }
            Console.ReadKey();
        }

    }
}

