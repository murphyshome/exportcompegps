This program exports the compegps flightbook to igc track files named YYYY-MM-dd_hh.mm_startplace ( skytraxx 3.0 naming format ).
 Igc files are placed in a directory structure YYYY/MM/dd. You can easily import those flights
 into the AirTome freeware or copy them onto your Skytraxx 3.0 variometer where they can be read
 by the builtin statistics.

 You might have entries in your logbook without gps track. For those entries dummy igc tracks are created that
 have the flight duration and start place found in the logbook.
 Most of my older tracks had the "trk" format. If a gps track has the "trk" ( compegps ) format, it will be converted to an igc track. 
 Comments that are in the flightbook and/or the tracks will be translated into one line and put into the "pilot"
 field. This field has been chosen because the AirTome flightbook can read and display that field.

 The program tries to find out the name of the site where you did start. If you have written a name into the flightbook
 no changes will be made to that name. If there is no name but a gps track, the program reads the first coordinates
 of the igc track and looks for the closest paragliding start place it can find. Igc files will be named after that
 place and the name of the startplace will also be put into the igc file.
 You must download at least one of the wpt files from dhv Germany (worldwide coverage) or flyland Switzerland (covers Switzerland only).
 
 Please note because this program changes fields in the igc tracks, those tracks won't have a valid g record any more.
 


### How do I get set up? ###

Download the solution and compile it.
Place the binary named "exportCompegps.exe" and the LOGBOOK.MDB file from compegps into
the same directory. Add two wpt file named "dhvgelaende_gpx_alle.gpx" and
"Waypoints_Startplatz.GPX" to the same directory. These are the waypoint files in gpx format from
the german dhv and flyland.ch. IGC files will be placed in a directory ..\flights when you
run the program from the directory where the LOGBOOK.MDB is placed.

* Dependencies
.net dependencies 

* Database configuration
The mdb file will be read using adodb. No further setup actions are required.

* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact